# stand

An editable, filterable and sortable spreadsheet (via [Ag Grid](https://github.com/PablocFonseca/streamlit-aggrid/)) webapp (via [Streamlit](https://streamlit.io/)) with REST API (via [CherryPy](https://cherrypy.dev/)) using [Pandas](https://pandas.pydata.org/) [DataFrames](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html).

<img src="https://gitlab.psi.ch/augustin_s/stand/-/wikis/uploads/ea8ad99871c9f2fd8fcdff38da028211/screenshot.png" width="50%" />

## Setup the environment

```bash
conda create -n stand
conda activate stand

conda install cherrypy
conda install conda-forge::streamlit=1.9.2
conda install openpyxl # excel writing
conda install pytables # hdf5 writing
conda install conda-forge::python-dotenv pyyaml simplejson # aggrid deps

pip install streamlit-aggrid
```

## Start the webapp

```bash
$ ./stand.py
```

Check the parameters with

```bash
$ ./stand.py -h
```

## Interact in the browser

- **edit**: double click a cell

- **filter**: hover a column head and click the appearing menu icon

- **sort**: click a column head (cycles through ascending, descending, chronological)

- **download**: click the *Downloads* button and choose from the given options

## Use the python client

```python
from client import Client

c = Client()

c.add_row(a=1, b=2.3, c="three")
c.add_row(a=2, b=3.4, c="four", x=123)

c.clear()
```

