import cherrypy as cp

from utils.dfh import DateFrameHolder
from utils.st_utils import rerunall, get_session_ids


@cp.expose
class TableController:

    def __init__(self):
        self.dfh = DateFrameHolder("output.h5")
#        self.changed = True
        self.sids = get_session_ids()

    def get(self, sid):
        changed = self.get_changed(sid)
        data = self.get_data(sid)
        return changed, data

    def get_changed(self, sid):
        return (sid in self.sids)

    def get_data(self, sid):
#        self.changed = False
        try:
            self.sids.remove(sid)
        except KeyError as e:
            if self.sids:
                raise RuntimeError(f"{self.sids} is not empty") from e
        return self.dfh.df

    def set_data(self, df):
        if self.dfh.df.equals(df):
            print("<<< skipping dump because dataframe did not change")
            return
        self.dfh.df = df
        self.dfh.dump()
        self._trigger_changed()

    def GET(self):
        return str(self.dfh.df)

    @cp.tools.json_in()
    def PATCH(self, **kwargs):
        kwargs = kwargs or cp.request.json
        self.dfh.append(kwargs)
        self._trigger_changed()
        self.dfh.dump()
        return str(self.dfh.df)

    def DELETE(self):
        self.dfh.clear()
        self._trigger_changed()
        return "cleared"


    def _trigger_changed(self):
#        self.changed = True
        self.sids = get_session_ids()
        rerunall()



