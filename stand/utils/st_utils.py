import streamlit as st


get_server = st.server.server.Server.get_current


def rerunall():
    sibi = get_session_info_by_id()
    for si in sibi.values():
        print("rerun:", si.session.id)
        client_state = None
        si.session.request_rerun(client_state)


def get_session_ids():
    sibi = get_session_info_by_id()
    sids = sibi.keys()
    return set(sids)


def rerun(session_id=None):
    if session_id is None:
        session_id = get_session_id()

    server = get_server()
    session = server.get_session_by_id(session_id)

    client_state = None
    session.request_rerun(client_state)


def get_session_id():
    ctx = st.scriptrunner.script_run_context.get_script_run_ctx()
    return ctx.session_id


def get_session_info_by_id():
    server = get_server()
    return server._session_info_by_id



def hide_UI_elements(menu=True, header=True, footer=True):
    HIDDEN = " {visibility: hidden;}"
    res = []

    if menu:
        res.append("#MainMenu" + HIDDEN)

    if header:
        res.append("header" + HIDDEN)

    if footer:
        res.append("footer" + HIDDEN)

    if not res:
        return

    res = ["<style>"] + res + ["</style>"]
    res = "\n".join(res)
    return st.markdown(res, unsafe_allow_html=True)



