from io import BytesIO


def to_excel_binary(df, **kwargs):
    with BytesIO() as out:
        df.to_excel(out, **kwargs)
        res = out.getvalue()
    return res



from pandas import HDFStore


def to_hdf_binary(df):
    with HDFStore(
        "wontbewritten.h5",
        mode="a",
        driver="H5FD_CORE",
        driver_core_backing_store=0
    ) as out:
        out["/data"] = df
        res = out._handle.get_file_image()
    return res



