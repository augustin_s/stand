import pandas as pd


class DateFrameHolder:

    def __init__(self, fn):
        self.fn = fn
        self.df = try_load_df(fn)

    def dump(self):
        dump_non_empty_df(self.df, self.fn)


    def append(self, data):
        data = pd.DataFrame.from_records([data])
        self.df = pd.concat([self.df, data], ignore_index=True)

    def clear(self):
        self.df = pd.DataFrame()


    def __repr__(self):
        head = self.fn + ":"
        line = "-" * len(head)
        df = str(self.df)
        return "\n".join((head, line, df))



def try_load_df(fn):
    try:
        df = pd.read_hdf(fn)
        print(f">>> loaded dataframe from {fn}")
    except FileNotFoundError:
        df = pd.DataFrame()
        print(">>> created empty dataframe")
    return df

def dump_non_empty_df(df, fn, key="data"):
    if df.empty:
        print("<<< skip dumping empty dataframe")
        return
    print(f"<<< dump dataframe to {fn}")
    df.to_hdf(fn, key)
    backup(df, fn, key) #TODO: test then remove



# the following is just a safety precaution
# if everything works, it can be removed

from datetime import datetime
from pathlib import Path

FMT = "%Y-%m-%d_%H-%M-%S-%f"
BAKDIR = ".backup"

def backup(df, fn, key):
    Path(BAKDIR).mkdir(parents=True, exist_ok=True)
    bfn = backup_filename(fn)
    print(f"<<< backup dataframe to {bfn}")
    df.to_hdf(bfn, key)

def backup_filename(fn):
    p = Path(fn)
    fn = p.stem
    ext = p.suffix
    ts = datetime.now().strftime(FMT)
    return f"{BAKDIR}/{fn}_{ts}{ext}"



