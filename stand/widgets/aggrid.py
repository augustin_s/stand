from st_aggrid import AgGrid, GridOptionsBuilder


def aggrid(df, reload_data=False, height="auto", pagination=True, key=None, **kwargs):
    df = df[::-1] # display in reversed chronological order

    gob = GridOptionsBuilder.from_dataframe(
        df,
        editable=True,
        filterable=True,
        groupable=True,
        resizable=True,
        sortable=True
    )

    auto_height = (height == "auto")
    gob.configure_auto_height(auto_height)
    gob.configure_pagination(pagination)

    go = gob.build()

    response = AgGrid(
        df,
        go,
        height=height,
        theme="streamlit",
        reload_data=reload_data,
        key=make_key(key, df, auto_height, pagination),
        **kwargs
    )

    df = response.get("data", df)
    df = df[::-1] # undo reversed chronological order

    #TODO: streamlit-aggrid changed return type
    if isinstance(response, dict):
        response["data"] = df # <= 0.2.3.post2
    else:
        response.data = df # >= 0.3.0

    return response



def make_key(prefix, df, auto_height, pagination):
    """
    encode the dataframe's column names into a key,
    as well as the state of the auto height and pagination settings,
    this triggers a hard reload (like F5) of the AgGrid if the columns/settings change
    """
    if prefix is None:
        return None
    items = list(df.columns)
    items.append(auto_height)
    items.append(pagination)
    return str(prefix) + ":" + "+".join(str(i) for i in items)



