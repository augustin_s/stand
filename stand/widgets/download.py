import streamlit as st

from utils.df2bin import to_excel_binary, to_hdf_binary


state = st.session_state


def download(df):
    showing_downloads = state.get("showing_downloads", False)

    col0, col1, col2, col3 = st.columns(4)
    if col0.button("Downloads") and not showing_downloads:
        state.showing_downloads = True

        h5 = to_hdf_binary(df)
        col1.download_button("📥 hdf5", h5, file_name="output.h5")

        xlsx = to_excel_binary(df)
        col2.download_button("📥 xlsx", xlsx, file_name="output.xlsx")

        csv = df.to_csv()
        col3.download_button("📥 csv", csv, file_name="output.csv")

#        st.stop()

    else:
        state.showing_downloads = False



