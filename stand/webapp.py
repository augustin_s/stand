from pathlib import Path

import streamlit as st

import hacks

from widgets.aggrid import aggrid
from widgets.download import download
from restapi import restapi
from utils.st_utils import get_session_id, hide_UI_elements


print(">>> start of streamlit run")

icon = Path(__file__).parent.parent / "icon.png"

st.set_page_config(
    page_title="stand",
    page_icon=str(icon),
    layout="wide",
    initial_sidebar_state="collapsed"
)

hide_UI_elements()


sid = get_session_id() # rest api needs current session IDs to trigger reruns
changed, df = restapi.get(sid)


download(df)


auto_height = st.sidebar.checkbox(
    "Auto Height",
    value=False,
    key="auto_height"
)

pagination = st.sidebar.checkbox(
    "Pagination",
    value=True,
    disabled=auto_height,
    key="pagination"
)

height = st.sidebar.slider(
    "Grid Height",
    min_value=100, max_value=1500, value=500, step=10,
    disabled=auto_height,
    key="grid_height"
)

if auto_height:
    pagination = False # would only show "1 of 1"
    height = "auto"


# if df in restapi changed, we need to reload it into aggrid.
# if df in restapi did not change, we do not reload to ensure changes in the browser persist.

response = aggrid(
    df,
    reload_data=changed,
    pagination=pagination,
    height=height,
    enable_enterprise_modules=False,
    key="stand"
)


# if we reloaded, aggrid returns the df from before (why?), thus we do not update the restapi.
# if we did not reload, aggrid may return an updated df from an edit in the browser,
# and thus we need to update the restapi.

new_df = response["data"]
if not new_df.equals(df) and not changed:
    restapi.set_data(new_df)


print(">>> end of streamlit run")



