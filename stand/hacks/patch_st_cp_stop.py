"""
Monkey patch the streamlit server to stop cherrypy upon its own stop
"""


import cherrypy
from streamlit.server.server import Server


old_fn = Server._on_stopped

def new_fn(*args, **kwargs):
    print("exit cherrypy")
    cherrypy.engine.exit()
    print("exit streamlit")
    return old_fn(*args, **kwargs)

Server._on_stopped = new_fn



