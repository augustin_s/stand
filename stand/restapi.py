import argparse
from threading import Thread

import cherrypy as cp

from tblctrl import TableController


parser = argparse.ArgumentParser()

parser.add_argument("--host", default="127.0.0.1")
parser.add_argument("--port", default=9090, type=int)

clargs = parser.parse_args()


cp.config.update({
    "server.socket_host": clargs.host,
    "server.socket_port": clargs.port,
})


# creating instances here allows to use restapi etc. as singletons

print(f">>> start restapi on {clargs.host}:{clargs.port}")

restapi = TableController()

conf = {
    "/": {
        "request.dispatch": cp.dispatch.MethodDispatcher(),
        "tools.sessions.on": True,
        "tools.response_headers.on": True,
        "tools.response_headers.headers": [("Content-Type", "text/plain")],
    }
}

args = (restapi, "/", conf)

thread = Thread(target=cp.quickstart, args=args, daemon=True)
thread.start()



