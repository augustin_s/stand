#!/bin/bash

RESET="\033[0m"
RED="\033[0;31m"
echo -e "${RED}please switch to the stand.py launcher${RESET}"

base=$(dirname $0)

params=(
  --browser.gatherUsageStats false
  --server.headless true
)

streamlit run "${params[@]}" $@ $base/stand/webapp.py


