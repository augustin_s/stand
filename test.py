#!/usr/bin/env python

from itertools import count, cycle
from random import random, randint
from string import ascii_letters
from time import sleep

from client import Client


def irandom():
    while True:
        yield random()

num = count()
ran = irandom()
let = cycle(ascii_letters)

c = Client()

c.clear()
sleep(1)

for i, r, l in zip(num, ran, let):
    l = l * randint(1, 5)

    kwargs = {} if i < 10 else {"x": 123}

    c.add_row(index=i, random=r, string=l, **kwargs)
    sleep(1)



