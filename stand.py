#!/usr/bin/env python

import argparse
import socket


parser = argparse.ArgumentParser(
    description="✋ stand",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)

hostname = socket.gethostname()

msg = " of the Streamlit app"
parser.add_argument("--app-host", default=hostname,       help="host"+msg)
parser.add_argument("--app-port", default=8080, type=int, help="port"+msg)

msg = " of the CherryPy API"
parser.add_argument("--api-host", default=hostname,       help="host"+msg)
parser.add_argument("--api-port", default=9090, type=int, help="port"+msg)

clargs = parser.parse_args()


streamlit_params = f"""
    --browser.gatherUsageStats false
    --server.headless true
    --server.address {clargs.app_host}
    --server.port {clargs.app_port}
""".split()

cherrypy_params = f"""
    --host {clargs.api_host}
    --port {clargs.api_port}
""".split()


import sys

# combine the folder that contains this script

base = sys.path[0] or "."
script = f"{base}/stand/webapp.py"


import subprocess

# arguments before "--" will be parsed by streamlit
# arguments after  "--" will be passed into the script
# the inner argparse is in restapi.py

cmd = [
#    "echo",
    "streamlit", "run", *streamlit_params, script,
    "--", *cherrypy_params
]

subprocess.run(cmd)



