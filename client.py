import requests
import json


class Client:

    def __init__(self, host="127.0.0.1", port=9090):
        self.host = host
        self.port = port
        self.session = requests.Session()

    def add_row(self, **kwargs):
        addr = self._make_addr()
        resp = self.session.patch(addr, json=kwargs)
        return ResponseWrapper(resp)

    def get(self):
        addr = self._make_addr()
        resp = self.session.get(addr)
        return ResponseWrapper(resp)

    def clear(self):
        addr = self._make_addr()
        resp = self.session.delete(addr)
        return ResponseWrapper(resp)

    def _make_addr(self):
        return f"http://{self.host}:{self.port}/"



class ResponseWrapper:

    def __init__(self, resp):
        resp.raise_for_status()
        self.resp = resp

    def __repr__(self):
        return self.resp.text



